import {inject, bindable} from 'aurelia-framework';
import {CarroService} from './services/carro';
import {ArticulosService} from './services/articulos';
import '../semantic/dist/components/popup.min';
import * as Blazy from '../node_modules/blazy/blazy.min';
// import './components/rating-custom-element';
// import '../components/rating';
@inject(CarroService, ArticulosService)
export class Articulos {
  @bindable articulos = null;
  carroService;
  constructor(carroService, articulosService) {
    this.carroService = carroService;
    this.articulosService = articulosService;
    this.ratingChange = this.ratingChangeFunction.bind(this);
  }

  addArticuloToCarrito(articulo, sub) {
    articulo.sub = sub;
    this.carroService.add(articulo);
    setTimeout(()=>{
      let d = $('#carro-div');
      d.scrollTop(d.prop('scrollHeight'));
    });
  }

  attached() {
    $('.popup').popup();
    Blazy.default();
  }


  ratingChangeFunction(rating, obj) {
    this.setRating(rating, obj);
  }

  setRating(rating, articulo) {
    this.articulosService.setRating(articulo, rating);
  }

}
