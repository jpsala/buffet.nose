import { inject, observable } from 'aurelia-framework';
import alertify from '../../node_modules/alertify.js/dist/js/alertify';
import {alertifyClose} from '../util';
import {Config} from '../config/config.js';
import Lector from '../services/lector';
import { SociosService } from '../services/socios';
import '../../semantic/dist/components/search.min';
import '../../semantic/dist/components/api';
import '../../semantic/dist/components/dropdown';
import '../../semantic/dist/semantic.min';
@inject(Lector, Config, SociosService)
export default class LectorEnroll{
  @observable esAdmin;
  @observable socio;
  cambioDeSocio;
  constructor(lector, config, sociosService){
    alertify.logPosition('top-right');
    this.lector = lector;
    this.config = config;
    this.sociosService = sociosService;
  }

  enroll(){
    alertify.log('Presione el dedo contra el detector de huellas tres veces durante dos segundos');
    this.lector.cmd('enroll', {'socio_id': Number(this.socio.id)}, 20000).then((msg)=>{
      if (msg.data.status){
        alertify.success('OK');
      } else {
        alertify.log('Error, mala lectura');
      }
    }).catch((e)=>{
      alertify.error('Error, timeout');
    });
  }

  esAdminChanged(esAdmin){
    if (this.cambioDeSocio) return;
    this.sociosService.setAdmin(this.socio.id, esAdmin);
  }

  socioChanged(socio){
    this.cambioDeSocio = true;
    this.esAdmin = Number(socio.admin) === 1;
    this.cambioDeSocio = false;
  }

  deleteTemplates(){
    alertify.confirm('Estás por borrar todas las huellas de la base de datos, seguro?', () => {
      this.lector.cmd('deleteTemplates');
    });
  }

  check(){
    // let alert = alertify.alert('Atención', 'Presione el dedo contra el detector de huellas hasta recibir un mensaje');
    alertify.alert('Presione el dedo contra el detector de huellas hasta recibir un mensaje');
    this.lector.cmd('check', {}, 10000).then((msg)=>{
      let encontrado = msg.data.resultado[0];
      console.log(msg.data && msg.data.resultado[0]);
      alertifyClose(2000);
      if (encontrado && Number(encontrado) !== -1){
        alertify.alert('Detección :'.concat(msg.data.resultado[0]));
        alertify.success('Detección :'.concat(msg.data.resultado[0]));
      } else {
        alertify.alert('Error', 'Detección :'.concat('No encontrado'));
        alertify.error('Detección :'.concat('No encontrado'));
      }
    }).catch((e)=>{
      $('.alertify').find('button').click();
      alertify.alert('Error', 'Error en lectura');
      alertify.error('Error en lectura');
      console.log('error:', e);
    });
  }

}
