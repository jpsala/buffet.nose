import {inject, observable} from 'aurelia-framework';
import {ArticulosService} from '../services/articulos';
import {CategoriasService} from '../services/categorias';
import {ImagenesService} from '../services/imagenes';
import {Config} from '../config/config';
// import '../../semantic/dist/components/dropdown.min';
import '../../semantic/dist/semantic.min';
@inject(ArticulosService, ImagenesService, CategoriasService)
export class Articulos {
  articulos;
  @observable articuloSelected;
  @observable imagenSelected;
  editando = false;
  articuloCopia;
  constructor(articulosService, imagenesService, categoriasService) {
    this.articulosService = articulosService;
    this.categoriasService = categoriasService;
    this.imagenesService = imagenesService;
    this.urlImages = Config.getUrlImages();
  }

  async activate() {
    this.articulos = await this.articulosService.getArticulos();
    this.articulos[0].categoria_id = 4;
    this.imagenes = await this.imagenesService.getImages();
    this.categorias = await this.categoriasService.getCategorias();
  }

  getCategoriaNombrePorId(categoriaId) {
    let categoria = this.categorias.find(c=>c.id === categoriaId);
    return categoria ? categoria.nombre : 'no encontrada';
  }

  selectArticulo(e, articulo) {
    if (!this.editando) {
      this.articuloCopia = Object.assign({}, articulo);
      this.editando = true;
      $(e.target).closest('tr').find('.ui.dropdown').dropdown();
      this.articuloSelected = articulo;
    }
  }

  imagenChange(event, imagen, articulo) {
    articulo.imagen = event.target.value;
    // articulo.imagenPath = this.urlImages + event.target.value;
  }

  categoriaChange(event, imagen, categoria) {
    // categoria.imagen = event.target.value;
    // categoria.imagenPath = this.urlImages + event.target.value;
  }

  focus(e, c) {
    if (this.editando) {
      return;
    }
    let $input = $(e.target).closest('td').find('input').first();
    setTimeout(()=>{
      $input.focus();
    }, 50);
  }

  grabaEdicionArticulo(e, articulo) {
    this.articuloSelected = null;
    this.articulosService.save(articulo);
    //TODO: this.articuloCopia = undefined;
    setTimeout(()=>{
      this.editando = false;
    });
  }

  cancelaEdicionArticulo(e, articulo) {
    this.articuloSelected = null;
    Object.assign(articulo, this.articuloCopia);
    setTimeout(()=>{
      this.editando = false;
    });
  }
}
