import {inject, observable} from 'aurelia-framework';
import {CategoriasService} from '../services/categorias';
import {ImagenesService} from '../services/imagenes';
import {Config} from '../config/config';
// import '../../semantic/dist/components/dropdown.min';
import '../../semantic/dist/semantic.min';
@inject(CategoriasService, ImagenesService)
export class Categorias {
  categorias;
  @observable categoriaSelected;
  @observable imagenSelected;
  editando = false;
  categoriaCopia;
  constructor(categoriasService, imagenesService) {
    this.categoriasService = categoriasService;
    this.imagenesService = imagenesService;
    this.urlImages = Config.getUrlImages();
  }

  async activate() {
    this.categorias = await this.categoriasService.getCategorias();
    this.imagenes = await this.imagenesService.getImages();
  }

  selectCategoria(e, categoria) {
    if (!this.editando) {
      this.categoriaCopia = Object.assign({}, categoria);
      this.editando = true;
      $(e.target).closest('tr').find('.ui.dropdown').first().dropdown();
      this.categoriaSelected = categoria;
    }
  }

  imagenChange(event, imagen, categoria) {
    categoria.imagen = event.target.value;
    categoria.imagenPath = this.urlImages + event.target.value;
  }

  focus(e, c) {
    if (this.editando) {
      return;
    }
    let $input = $(e.target).closest('td').find('input').first();
    setTimeout(()=>{
      $input.focus();
    }, 50);
  }

  grabaEdicionCategoria(e, categoria) {
    this.categoriaSelected = null;
    this.categoriasService.save(categoria);
    setTimeout(()=>{
      this.editando = false;
    });
  }

  cancelaEdicionCategoria(e, categoria) {
    this.categoriaSelected = null;
    Object.assign(categoria, this.categoriaCopia);
    setTimeout(()=>{
      this.editando = false;
    });
  }
}
