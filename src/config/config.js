export class Config {
  static local = document.location.hostname === 'localhost';

  static getUrlBase() {
    // console.log(document.location.hostname);
    // return 'http://' + '192.168.2.254' + '/iae/';
    return 'http://' + document.location.hostname + '/iae/';
  }

  static getUrlApi() {
    return Config.getUrlBase() + '?r=buffetApi';
  }

  static getUrlImages() {
    return 'http://' + document.location.hostname + '/buffet/images/';
  }
}
