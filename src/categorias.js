import {inject} from 'aurelia-framework';
import {Router} from 'aurelia-router';
import {CategoriasService} from './services/categorias';
@inject(CategoriasService, Router)
export class Categorias {
  categoriasService;
  categorias = [];

  constructor(categoriasService, router) {
    this.categoriasService = categoriasService;
    this.router = router;
  }

  async activate() {
    this.categorias = await this.categoriasService.getCategorias();
  }

  menuGo(categoriaId) {
    this.router.navigate(`menu/${categoriaId}`);
  }
}
