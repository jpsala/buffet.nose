import numeral from 'numeral';
export class NumberFormatValueConverter {
  toView(value, format = '$0,0.00') {
    if (!value){
      return null;
    }
    return numeral(value).format(format);
  }
}
