import numeral from 'numeral';
export class CurrencyFormatValueConverter {
  toView(value = '', format = '$0,0.00') {
    if (value === '') return '';
    if (isNaN(value)) value = 0;
    return numeral(value).format(format);
  }
}
