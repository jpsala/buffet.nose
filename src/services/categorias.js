import {HttpClient, json} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';
import {Config} from './../config/config';
@inject(HttpClient)
export class CategoriasService {
  _categorias = [];

  constructor(http) {
    this.http = http;
  }

  async getCategorias() {
    if (this._categorias.length === 0) {
      return await this
                .http
                .fetch('/categorias')
                .then(r=>r.data.map((e)=> {
                  e.imagenPath = e.imagen.trim() ? `${Config.getUrlImages()}/${e.imagen.trim()}` : null;
                  return e;
                }))
                .then(a => {
                  this._categorias = a;
                  return a;
                }).catch(reason => alert(reason));
    }
    return new Promise((fulfill) => {
      fulfill(this._categorias);
    });
  }

  async getCategoriaPorID(categoriaId) {
    let categoria;
    let categorias;
    if (categoriaId) {
      categorias = await this.getCategorias();
      categoria = categorias.find(ret=>ret.id === categoriaId);
    } else {
      categoria = {id: -1, nombre: 'Todos', imagen: false};
    }

    return categoria;
  }

  save(categoria) {
    return this
            .http
            .fetch('/grabaCategoria', {
              body: json(categoria)
            })
            .then((r)=>r.id);
  }

  add(categoria) {
    this._categorias.push(categoria);
  }

  del(categoria) {
    return this
            .http
            .fetch('/delCategoria', {
              body: json(categoria)
            });
  }
}
