import {HttpClient, json} from 'aurelia-fetch-client';
import {inject} from 'aurelia-framework';
import {ArticuloModel} from '../models/articulo';
import AuthService from '../services/auth';
import {CategoriasService} from '../services/categorias';
@inject(HttpClient, ArticuloModel, CategoriasService, AuthService)
export class ArticulosService {
  _articulos = [];
  _ratings = [];
  categoria = 'Todos';

  constructor(http, articuloModel, categoriasService, authService) {
    this.http = http;
    this.articuloModel = articuloModel;
    this.categoriasService = categoriasService;
    this.authService = authService;
  }

  async getArticulos() {
    if (this._articulos.length === 0) {
      await this.getRatings();
      return this._articulos = await this
                .http
                .fetch('/articulos')
                .then(r=>{
                  return r.data.map(e=>this.articuloModel.nuevo(e));
                })
                .then((articulos)=>{
                  articulos.forEach(art=>{
                    let rating = this._ratings.find(rat=>
                      rat.articulo_id === art.id
                    );
                    art.rating = rating ? rating.rating : 0;
                  });
                  return articulos;
                });
    }
    return await new Promise((fulfill)=> {
      fulfill(this._articulos);
    });
  }

  async getRatings() {
    if (this._ratings.length === 0) {
      return await this.http.fetch('/ratings', {
        body: json({socio_id: this.authService.user.socio_id})
      })
      .then(r=>this._ratings = r.ratings);
    }
    return this._ratings;
    // return await new Promise((fulfill)=> {
    //   fulfill(this._ratings);
    // });
  }

  async setRating(articulo, rating) {
    articulo.rating = rating;
    this.http.fetch('/ratings_set', {
      body: json({
        socio_id: this.authService.user.socio_id,
        articulo_id: articulo.id,
        rating: rating
      })
    });
  }

  async getArticulosFiltrados(categoriaId) {
    if (categoriaId) {
      this.categoria = await this.categoriasService.getCategoriaPorID(categoriaId);
    } else {
      this.categoria = 'Todos';
      return this.getArticulos();
    }
    let articulos = await this.getArticulos().then(articulos=> {
      return articulos.filter((a) => {
        return a.categoria_id === categoriaId;
      });
    });
    return articulos || [];
  }

  save(articulo) {
    let artParaGrabar = this.articuloModel.limpia(articulo);
    return this
            .http
            .fetch('/graba', {
              body: json(artParaGrabar)
            })
            .then(r => r.id);
  }

  add(articulo) {
    this._articulos.push(articulo);
  }

  del(articulo) {
    let artParaGrabar = this.articuloModel.limpia(articulo);
    return this
            .http
            .fetch('/del', {
              body: json(artParaGrabar)
            });
  }
}
