import {inject, observable} from 'aurelia-framework';
import {EventAggregator} from 'aurelia-event-aggregator';
import {Socket} from './socket';
@inject(EventAggregator, Socket)
export default class Lector {
  @observable ready = false;
  cmdWaiting = false;
  reconnectIntervalHandler = undefined
  handleTimeout;
  handleMessageSubscription;
  constructor(ea, socket){
    this.ready = false;
    this.ea = ea;
    this.socket = socket;
    this.onReadyChangedCallBack = undefined;
    this.ea.subscribe('socket_message', this.onrecived.bind(this));
    this.ea.subscribe('socket_disconnected', this.ondisconnected.bind(this));
    this.ea.subscribe('socket_connected', this.onconnected.bind(this));
  }

  // por cada mensaje recibido publico un evento
  onrecived(response){
    let msg = JSON.parse(response);
    this.ea.publish('socket_message_'.concat(msg.cmd), msg);
  }

  ondisconnected(response){
    this.ready = false;
  }

  onconnected(response){
    this.chkLectorReady();
  }

  chkLectorReady(){
    this.cmd('initEngine').then((msg)=>{
      this.ready = msg.data.ready;
    }).catch((e)=>{
      this.ready = false;
    });
  }

  readyChanged(){
    if (typeof this.onReadyChangedCallBack === 'function'){
      this.onReadyChangedCallBack(this.ready);
    }
    if (this.ready){
      this.autoReconnectOff();
    } else {
      this.autoReconnectOn();
    }
  }

  autoReconnectOn(){
    if (!this.reconnectIntervalHandler){
      this.reconnectIntervalHandler = setInterval(()=>{this.chkLectorReady();}, 5000);
    }
  }

  autoReconnectOff(){
    if (this.reconnectIntervalHandler){
      clearInterval(this.reconnectIntervalHandler);
      this.reconnectIntervalHandler = undefined;
    }
  }

  async connectToSocket(){
    return await this.socket.connect();
  }

  async refreshData(){
    await this.cmd('refreshData', {}, 10000);
  }

  cancelCmd(){
    this.ea.publish('socket_message', JSON.stringify({cmd: 'cancelCmd', data: {}}));
  }

  cmdNoWait(cmd, data){
    if (! this.socket.connected){
      console.log('cmd', 'Socket desconectado');
      return false;
    }
    this.socket.send({'cmd': cmd, 'data': data});
  }

  async cmd(cmd, data = {}, timeout = 1000){
    // console.log('cmd', cmd);

    if (! this.socket.connected){
      // console.log('cmd', 'Socket desconectado');
      throw Error('Socket desconectado');
    }

    if (this.cmdWaiting){ //ya estoy ejectando un comando, throw an error
      console.log('cmd', 'Ya estoy esperando por un comando ('.concat(this.cmdWaiting).concat(')'), this.handleMessageSubscription, this.handleTimeout);
      throw Error('Ya estoy esperando por un comando ('.concat(this.cmdWaiting).concat(')'));
    }

    let disposeAll = function(){
      console.log('disposing', this.cmdWaiting, this.handleMessageSubscription, this.handleTimeout);
      this.cmdWaiting = undefined;
      this.handleMessageSubscription.dispose();
      this.handleMessageSubscription = undefined;
      clearTimeout(this.handleTimeout);
      this.handleTimeout = undefined;
      console.log('disposed', this.cmdWaiting, this.handleMessageSubscription, this.handleTimeout);
    }.bind(this);

    return await new Promise((resolve, reject)=>{
      this.socket.send({'cmd': cmd, 'data': data});
      this.cmdWaiting = cmd;
      this.handleMessageSubscription = this.ea.subscribe('socket_message', (rawmsg)=>{
        console.log('rawmsg %o', rawmsg);
        let msg = JSON.parse(rawmsg);
        if (msg.cmd === cmd){ //recibido el mensaje correcto, resolve promise and dispose
          console.log('message ok, disposeAll()');
          msg.other = false;
        } else if (msg.cmd === 'cancelCmd'){
          reject('Command canceled: '.concat(this.cmdWaiting));
        } else {
          msg.other = true;
          console.log('message recived but it\'s not '.concat(this.cmdWaiting).concat(', is '), msg);
        }
        resolve(msg);
        disposeAll();
      });
      this.handleTimeout = setTimeout(()=>{ //timeout, reject promise and dispose
        console.log('timeout en cmd para '.concat(this.cmdWaiting).concat(' en ').concat(timeout));
        disposeAll();
        reject('Timeout para '.concat(cmd));
      }, timeout);
    });
  }
}
