export function alertifyClose(timeout = 0){
  setTimeout(()=>{
    $('.alertify').find('button').click();
  }, timeout);
}
