import {bindable, inject, customElement} from 'aurelia-framework';
import '../semantic/dist/components/rating.min';
import '../node_modules/semantic-ui-transition/transition.min';

// Import JSPM modules we installed earlier

@customElement('rating') // Define the name of our custom element
@inject(Element) // Inject the instance of this element
// @noView()
// @containerless()
export class Rating {
  @bindable change
  @bindable stars
  @bindable obj
  $el = undefined;

  constructor(element) {
    this.element = element;
  }

  attached() {
    this.$el = $(this.element).find('div');
    this.$el.addClass('ui star rating');
    this.$el.rating({maxRating: 5, onRate: (r)=>this.stars = r });
  }

  remove() {
    this.stars = 0;
  }

  starsChanged(a) {
    if (this.$el) {
      this.$el.rating('set rating', this.stars);
      if (typeof this.change === 'function') {
        this.change(this.stars, this.obj, this.element);
      }
    }
  }
}
